#!/bin/bash

# Number of API requests to send
num_requests=5

# API endpoint URL
api_url="http://localhost:12000/example"

# Loop to send API requests
for ((i=1; i<=$num_requests; i++)); do

    request_data="{\"RequestID\": $i}"
    status_code=$(curl -s -o /dev/null -w "%{http_code}" -X POST -H "Content-Type: application/json" -d "$request_data" "$api_url")

    echo "RequestID: $i - Status Code: $status_code"
done
